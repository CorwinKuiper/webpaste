package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path[1:] == "" {
		Index(w, r)
	} else {
		log.Println("Got a request for :", r.URL.Path[1:])
		Page(w, r)
	}
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}

//Return a page from pastebin
func Page(w http.ResponseWriter, r *http.Request) {
	resp, err := http.Get(fmt.Sprintf("http://pastebin.com/raw/%s", r.URL.Path[1:]))
	if err != nil {
		fmt.Fprintf(w, "There was an error: %s", err.Error())
	} else {
		defer resp.Body.Close()
		body, _ := ioutil.ReadAll(resp.Body)
		fmt.Fprintf(w, "%s", body)
	}
}

func Index(w http.ResponseWriter, r *http.Request) {
	index, _ := ioutil.ReadFile("index.html")
	fmt.Fprintf(w, "%s", index)
}
